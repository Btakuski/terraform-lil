provider "google" {
  credentials = "${file("../gcp-acct.json")}"
  project = "terraform-lil-btakuski"
  region = "us-east1"
}

provider "aws" {
  region = "us-east-2"
}

provider "azurerm" {
  subscription_id = "${var.subscription_id}"
  client_id = "${var.client_id}"
  client_secret = "${var.client_secret}"
  tenant_id = "${var.tenant_id}"
}

variable subscription_id {}
variable client_id {}
variable client_secret {}
variable tenant_id {}